import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mkonnekt_kitchen/data/my_colors.dart';
import 'package:mkonnekt_kitchen/data/my_strings.dart';
import 'package:mkonnekt_kitchen/data/my_text.dart';
import 'package:mkonnekt_kitchen/data/utills.dart';

class OurLocation extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    return new OurLocationState();
  }
}

class OurLocationState extends State<OurLocation> {
  TextEditingController locationAddress = new TextEditingController();
  ScrollController weekTimingsList = new ScrollController();

  @override
  Widget build(BuildContext context) {
    return new SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Container(
            child: Card(
              margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
              child: Container(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                  width: double.infinity,
                  height: 80,
                  child: Center(
                      child: Image.asset(
                        Utility.getImage('food_konnekt_logo.png'),
                        fit: BoxFit.fill,
                      )
                  )
              ),
            ),
            decoration: new BoxDecoration(
              boxShadow: [
                new BoxShadow(
                  color: Colors.black12,
                  blurRadius: 21.1,
                ),
              ],
            ),
          ), // toolbar icon
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text(MyStrings.OUR_LOCATION,
                  style: MyText.headline(context).copyWith(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
              ),
            ),
          ), // Title Our location
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              child: Text(
                  "3333 W, Camp Wisdom Rd #118 Dallas\nTX 75377",
                  style: MyText.body1(context).copyWith(
                    color: Colors.black26,
                    fontWeight: FontWeight.normal,
                  ),
              ),
            ),
          ), // Address
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5),
            child: Container(
              height: 1.0,
              width: double.infinity,
              color: Colors.black26,
            ),
          ), // Divider line
          Align(
            alignment: Alignment.center,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      child: FlatButton(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0), side: BorderSide(color: MyColors.primary)),
                        onPressed: () {
                          Utility.launchCaller('8827293364');
                        },
                        child: Text(
                          MyStrings.CALL_US,
                          style: MyText.body2(context).copyWith(
                            color: MyColors.primary,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ), // FlatButton Call Us
                  Expanded(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      child: FlatButton(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0), side: BorderSide(color: MyColors.primary)),
                        onPressed: () {

                        },
                        child: Text(
                          MyStrings.FIND_US,
                          style: MyText.body2(context).copyWith(
                            color: MyColors.primary,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ), // FlatButton Find Us
                ]
            ),
          ), // Buttons Call Us /Find Us
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              margin: EdgeInsets.fromLTRB(20, 10, 0, 0),
              child: Text(
                  MyStrings.CONNECT_WITH_US,
                  style: MyText.body1(context).copyWith(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                  )),
            ),
          ), // Connect with us
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  GestureDetector(
                    child: Container(
                      height: 30, width: 30,
                      margin: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                        color: MyColors.primary
                      ),
                      child: Image.asset(
                        Utility.getImage('ic_facebook.png'),
                        fit: BoxFit.fill,
                      ),
                    ),
                    onTap:  (){
                      Utility.launchFacebooPage('shubham.bhawsar.944');
                    },
                  ), // button facebook page
                  GestureDetector(
                    child: Container(
                      height: 30, width: 30,
                      margin: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                          color: MyColors.primary
                      ),
                      child: Image.asset(
                        Utility.getImage('ic_instagram.png'),
                        fit: BoxFit.fill,
                      ),
                    ),
                    onTap:  (){
                      Utility.launchInstagram('shubham.bhawsar_/');
                    } ,
                  ), // button instagram page
                  GestureDetector(
                    child: Container(
                      height: 30, width: 30,
                      margin: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                          color: MyColors.primary
                      ),
                      child: Image.asset(
                        Utility.getImage('ic_link.png'),
                        fit: BoxFit.fill,
                      ),
                    ),
                    onTap:  (){
                    } ,
                  ),
                  GestureDetector(
                    child: Container(
                      height: 30, width: 30,
                      margin: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                          color: MyColors.primary
                      ),
                      child: Image.asset(
                        Utility.getImage('ic_twitter.png'),
                        fit: BoxFit.fill,
                      ),
                    ),
                    onTap:  (){
                      Utility.launchTwitter('_Shubham_B');
                    } ,
                  ), // button twitter page
                ]
            ),
          ), // Connect buttons (facebook, instagram, twitter)
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              margin: EdgeInsets.fromLTRB(20, 10, 0, 0),
              child: Text(
                  MyStrings.WORKING_HOURS,
                  style: MyText.medium(context).copyWith(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                  )),
            ),
          ), // Connect with us
          ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              controller: weekTimingsList,
              itemCount: 7,
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              primary: false,
              itemBuilder: (BuildContext context, int position) {
                return getRow(position);
              }
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.all(30),
            child: FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                  side: BorderSide(color: MyColors.primary)),
              onPressed: () {

              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  IconTheme(
                      data: new IconThemeData(
                          color: MyColors.primary),
                      child: Icon(Icons.location_on , size: 14)),
                  Text(
                    MyStrings.FIND_YOUR_NEAR_BY_STORE,
                    style: MyText.body2(context).copyWith(
                      color: MyColors.primary,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
          ), // FlatButton find near by store
        ],
      ),
    );
  }

  Widget getRow(int i) {
    return GestureDetector(
      child: Padding(
          padding: EdgeInsets.all(5.0),
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    flex: 1,
                    child: Text(
                        "Monday",
                        style: MyText.body1(context).copyWith(
                          color: Colors.black26,
                          fontWeight: FontWeight.normal,
                        )),
                  ),

                  Expanded(
                    flex: 2,
                    child: Text(
                        "10:00 am - to - 07:30 pm",
                        style: MyText.body1(context).copyWith(
                          color: Colors.black26,
                          fontWeight: FontWeight.normal,
                        )),
                  ),
                ]
            ),
          ) ,
      ),
      onTap: () {
        setState(() {
          print('row $i');
        });
      },
    );
  }
}