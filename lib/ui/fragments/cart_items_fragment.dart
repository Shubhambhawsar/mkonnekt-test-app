import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mkonnekt_kitchen/data/my_colors.dart';
import 'package:mkonnekt_kitchen/data/my_text.dart';
import 'package:mkonnekt_kitchen/data/utills.dart';

class CartItemList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new CartItemListState();
  }
}

class CartItemListState extends State<CartItemList> {
  ScrollController scrollController = new ScrollController();
  int itemCount = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
                physics: ScrollPhysics(),
                controller: scrollController,
                itemCount: 6,
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                primary: false,
                itemBuilder: (BuildContext context, int position) {
                  return getCartItemRow(position);
                },
            ),
          ),
        ],
      ),
    );
  }

  Widget getCartItemRow(int i) {
    return GestureDetector(
      child: Card(
        elevation: 1,
        color: Colors.white,
        margin: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
        child: Row(
          children: [
            Expanded(
              flex: 1,
              child: Container(
                  width: double.infinity,
                  child: Image.asset(
                    Utility.getImage('pizza.jpg'),
                    fit: BoxFit.fill,
                  )),
            ),
            Expanded(
              flex: 3,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              "Item Name",
                              maxLines: 1,
                              style: MyText.body2(context).copyWith(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Align(
                            alignment: Alignment.topRight,
                            child: Text(
                              "\$ 64.11",
                              maxLines: 1,
                              style: MyText.body1(context).copyWith(
                                  color: MyColors.primary,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: GestureDetector(
                            child: Container(
                              margin: EdgeInsets.all(10.0),
                                decoration: BoxDecoration(
                                    color: MyColors.primary,
                                    borderRadius: BorderRadius.horizontal(
                                        left: Radius.circular(10.0),
                                        right: Radius.circular(10.0))),
                                child: Icon(
                                  Icons.remove,
                                  color: Colors.white,
                                )),
                            onTap: () {
                              if (itemCount > 1) itemCount = itemCount - 1;
                              setState(() {});
                            },
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Align(
                              alignment: Alignment.center,
                              child: Text(
                                itemCount.toString(),
                                style: MyText.body2(context).copyWith(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              )),
                        ),
                        Expanded(
                          flex: 1,
                          child: GestureDetector(
                            child: Container(
                                margin: EdgeInsets.all(10.0),
                                decoration: BoxDecoration(
                                    color: MyColors.primary,
                                    borderRadius: BorderRadius.horizontal(
                                        left: Radius.circular(10.0),
                                        right: Radius.circular(10.0))),
                                child: Icon(
                                  Icons.add,
                                  color: Colors.white,
                                )),
                            onTap: () {
                              itemCount = itemCount + 1;
                              setState(() {});
                            },
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
