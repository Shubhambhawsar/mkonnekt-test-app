import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mkonnekt_kitchen/data/my_colors.dart';
import 'package:mkonnekt_kitchen/data/my_strings.dart';
import 'package:mkonnekt_kitchen/data/my_text.dart';
import 'package:mkonnekt_kitchen/data/utills.dart';

class OffersList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new OffersListState();
  }
}

class OffersListState extends State<OffersList> {
  ScrollController scrollController = new ScrollController();
  TextEditingController mobileNumber = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            child: Card(
              margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
              child: Column(
                children: [
                  Container(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                      width: double.infinity,
                      height: 80,
                      child: Center(
                          child: Image.asset(
                            Utility.getImage('food_konnekt_logo.png'),
                            fit: BoxFit.fill,
                          ))),
                ],
              ),
            ),
            decoration: new BoxDecoration(
              boxShadow: [
                new BoxShadow(
                  color: Colors.black12,
                  blurRadius: 21.1,
                ),
              ],
            ),
          ), // toolbar icon
          Expanded(
            child: ListView.builder(
                physics: ScrollPhysics(),
                controller: scrollController,
                itemCount: 11,
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                primary: false,
                itemBuilder: (BuildContext context, int position) {
                  return getRow(position);
                }
            ),
          ),
        ],
      ),
    );
  }

  Widget getRow(int i) {
    return GestureDetector(
      child: Padding(
          padding: EdgeInsets.all(5.0),
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                    border: Border.all(
                        color: MyColors.accent,
                        width: 1
                    )
                ),
                height: 110, width: double.infinity,
                child: Image.asset(
                  Utility.getImage('food_konnekt_logo.png'),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  width: 110,
                  margin: EdgeInsets.all(10.0),
                  child: FlatButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    color: Colors.blueAccent,
                    child: Text(
                      MyStrings.KNOW_MORE,
                      style: MyText.body2(context).copyWith(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onPressed: () {

                    },
                  ),
                ), // Know more button
              )
            ],
          )),
      onTap: () {
        setState(() {
          print('row $i');
        });
      },
    );
  }
}
