import 'dart:async';
import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinning_wheel/flutter_spinning_wheel.dart';
import 'package:mkonnekt_kitchen/data/my_text.dart';
import 'package:mkonnekt_kitchen/data/utills.dart';

class GamesScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new GamesScreenState();
  }
}

class GamesScreenState extends State<GamesScreen> {
  final StreamController _dividerController = StreamController<int>();
  double _generateRandomAngle() => Random().nextDouble() * pi * 2;

  dispose() {
    _dividerController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Card(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                child: Container(
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                    width: double.infinity,
                    height: 80,
                    child: Center(
                        child: Image.asset(
                          Utility.getImage('food_konnekt_logo.png'),
                          fit: BoxFit.fill,
                        ))),
              ),
              decoration: new BoxDecoration(
                boxShadow: [
                  new BoxShadow(
                    color: Colors.black12,
                    blurRadius: 21.1,
                  ),
                ],
              ),
            ), // toolbar icon
            Container(
              margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text("Hello! Spin the wheel..",
                  style: MyText.title(context).copyWith(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  )),
            ), // Hello wheel Glad to see you
            SpinningWheel(
              Image.asset('assets/images/wheel.png'),
              width: 310,
              height: 310,
              initialSpinAngle: _generateRandomAngle(),
              spinResistance: 0.6,
              canInteractWhileSpinning: false,
              dividers: 8,
              onUpdate: _dividerController.add,
              onEnd: _dividerController.add,
              secondaryImage:
              Image.asset('assets/images/wheel.png'),
              secondaryImageHeight: 110,
              secondaryImageWidth: 110,
            ),
            SizedBox(height: 30),
            StreamBuilder(
              stream: _dividerController.stream,
              builder: (context, snapshot) =>
              snapshot.hasData ? RouletteScore(snapshot.data) : Container(),
            )
          ],
        ),
      ),
    );
  }


}

class RouletteScore extends StatelessWidget {
  final int selected;

  final Map<int, String> labels = {
    1: '1000\$',
    2: 'Better luck next time!',
    3: '800\$',
    4: '7000\$',
    5: '5000\$',
    6: '300\$',
    7: '2000\$',
    8: '100\$',
    9: '50\$',
    10: '10\$',
    11: '400\$',
    12: '0\$',
  };

  RouletteScore(this.selected);

  @override
  Widget build(BuildContext context) {
    return Text('${labels[selected]}',
        style: TextStyle(fontStyle: FontStyle.italic, fontSize: 24.0));
  }
}
