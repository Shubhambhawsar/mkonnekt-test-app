import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mkonnekt_kitchen/services/payment_service.dart';

class CheckOutScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new CheckOutScreenState();
  }
}

class CheckOutScreenState extends State<CheckOutScreen> {
  final String _currentSecret = "pk_test_aSaULNS8cJU6Tvo20VAXy6rp";

  @override
  void initState() {
    super.initState();
    StripeService.init();
    //         publishableKey:"sk_test_ytGbEHvrNgJPs03sPJvXKP6j",
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Align(
        alignment: Alignment.center,
        child: GestureDetector(
          child: Text('Pay'),
          onTap: () async{
            var response = await StripeService.paymentViaNewCard('1101', 'INR');
            print('Response ' + response.message );
            if (response.success == true) {
              Scaffold.of(context).showSnackBar(
                  SnackBar(
                    content: Text(response.message), duration: Duration(microseconds: 1101)
                  )
              );
            } else
              Scaffold.of(context).showSnackBar(
                  SnackBar(
                      content: Text('Error: '+response.message), duration: Duration(microseconds: 1101)
                  )
              );
          }
        ),
      ),
    );
  }
}