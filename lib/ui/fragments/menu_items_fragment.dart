import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:mkonnekt_kitchen/data/my_colors.dart';
import 'package:mkonnekt_kitchen/data/my_strings.dart';
import 'package:mkonnekt_kitchen/data/my_text.dart';
import 'package:mkonnekt_kitchen/data/utills.dart';
import 'package:mkonnekt_kitchen/network/api_client.dart';
import 'package:mkonnekt_kitchen/network/model/models.dart';
import 'package:mkonnekt_kitchen/network/model/response/category_list.dart';
import 'package:mkonnekt_kitchen/network/model/response/menuItems.dart';
import 'package:mkonnekt_kitchen/ui/activities/my_cart.dart';

class MenuItemScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: MyMenuItems());
  }
}

class MyMenuItems extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MenuItemsState();
  }
}

class _MenuItemsState extends State<MyMenuItems> with TickerProviderStateMixin {
  Future<CategoryList> categoryListResponse;
  List<MenuItem> finalMenuItems;
  Future<MenuItemsList> menuItemsList;
  int categoryId = 5528, selectedCategoryPos = 0;

  @override
  void initState() {
    categoryListResponse = ApiClient.getMenuCategories('c4656bc2-cc41-4486-8d1e-1b4665c57fa7');
    menuItemsList = ApiClient.getItemsByCategory(categoryId.toString(), '301');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Container(
            child: Card(
              margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Container(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                  width: double.infinity,
                  height: 80,
                  child: Center(
                      child: Image.asset(Utility.getImage('food_konnekt_logo.png'), fit: BoxFit.fill))),
            ),
          ), // toolbar icon
          Container(
            color: MyColors.primary,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  flex: 8,
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
                    height: 40,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.horizontal(left: Radius.circular(10.0), right: Radius.circular(10.0)),
                    ),
                    child: TextFormField(
                      cursorColor: MyColors.primary,
                      keyboardType: TextInputType.text,
                      maxLines: 1,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        contentPadding: EdgeInsets.symmetric(horizontal: 15),
                        hintText: MyStrings.SEARCH_BY_NAME_ADDRESS,
                      ),
                      onChanged: onSearchTextChanged,
                    ),
                  ),
                ),
                Expanded(flex: 1,
                    child: Icon(Icons.search, color: Colors.white,)
                ),
                Expanded(
                  flex: 1,
                  child: GestureDetector(
                      child: Icon(Icons.add_shopping_cart, color: Colors.white,),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => MyCartScreen()));
                    },
                  ),
                )
              ],
            ),
          ),
          Container(
            width: double.infinity,
            height: 50,
            child: FutureBuilder<CategoryList>(
              future: categoryListResponse,
              builder: (context, snapshot) {
                if (snapshot.hasData && snapshot.data.status == 200) {
                  return ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: snapshot.data.categories.length,
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int position) {
                      // if( position== 0) {
                      //   categoryId = snapshot.data.categories[position].id;
                      // }
                      return getCategoryRow(
                          position, snapshot.data.categories[position]);
                    },
                  );
                } else
                  return Center(
                      child: CircularProgressIndicator(
                          backgroundColor: MyColors.primary));
              },
            ),
          ), // Category item horizontal list
          Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 20.0),
              child: FutureBuilder<MenuItemsList>(
                future: menuItemsList,
                builder: (context, snapshot) {
                  if (snapshot.hasData && snapshot.data.menuItems.length != 0) {
                    finalMenuItems = snapshot.data.menuItems;
                    return GridView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: snapshot.data.menuItems.length,
                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2),
                      itemBuilder: (BuildContext context, int position) {
                        return Container(
                            child: getMenuItemGridRow(
                                position, snapshot.data.menuItems[position]));
                      },
                    );
                  } else {
                    return Center(
                        child: CircularProgressIndicator(
                            backgroundColor: MyColors.primary));
                  }
                },
              ),
            ),
          ), // Category wise menu item list
        ],
      ),
    );
  }

  Widget getCategoryRow(int position, Category categoryObject) {
    TextStyle textStyle;
    return GestureDetector(
      child: Padding(
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: [
            Text(
              categoryObject.name,
              style: textStyle = TextStyle(
                fontSize: 15.0,
                fontWeight: FontWeight.normal,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 5), //top padding 5
              height: 5,
              width: Utility.getTextSize(categoryObject.name + 'xx', textStyle)
                  .width,
              color: selectedCategoryPos == position
                  ? MyColors.primary
                  : Colors.black12,
            )
          ],
        ),
      ),
      onTap: () {
        setState(() {
          selectedCategoryPos = position;
          categoryId = categoryObject.id;
        });
      },
    );
  }

  Widget getMenuItemGridRow(int position, MenuItem menuItem) {
    return GestureDetector(
      child: Container(
        width: 100,
        margin: EdgeInsets.all(5.0),
        child: Column(
          children: [
            Expanded(
              child: Align(
                  alignment: Alignment.center,
                  child: Container(
                      width: double.infinity,
                      child: Image.asset(
                        Utility.getImage('pizza.jpg'),
                        fit: BoxFit.fill,
                      ))),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 5.0),
              child: Row(
                children: [
                  Expanded(
                    flex: 3,
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        menuItem.name !=null ? menuItem.name: "Item Name",
                        maxLines: 1,
                        style: MyText.body2(context).copyWith(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Text(
                        menuItem.price.toString()!=null ? '\$ ' +  menuItem.price.toStringAsFixed(2): "",
                        maxLines: 1,
                        style: MyText.body1(context).copyWith(
                            color: MyColors.primary,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Text(
              menuItem.description!=null ? menuItem.description:"",
              maxLines: 2,
              style: MyText.caption(context)
                  .copyWith(color: Colors.black, fontWeight: FontWeight.normal),
            ),
            Container(
              width: double.infinity,
              height: 24,
              margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 20.0),
              child: FlatButton(
                color: MyColors.primary,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0)),
                onPressed: () {},
                child: Text(
                  MyStrings.ADD,
                  style: MyText.caption(context).copyWith(
                      color: Colors.white, fontWeight: FontWeight.normal),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }


  onSearchTextChanged(String text) async {
    List<MenuItem> searchRresult = new List();

    finalMenuItems.forEach((element) { 
      if(element.name.contains(text) || element.description.contains(text)) {
        searchRresult.add(element);
      }
    });
    menuItemsList.then((value) => value.menuItems = searchRresult);
    setState(() {});
  }
}
