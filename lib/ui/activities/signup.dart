import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:mkonnekt_kitchen/data/my_colors.dart';
import 'package:mkonnekt_kitchen/data/my_strings.dart';
import 'package:mkonnekt_kitchen/data/my_text.dart';
import 'package:mkonnekt_kitchen/data/utills.dart';
import 'home.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as JSON;

class SignUpScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return new SignUpScreenState();
  }
}

class SignUpScreenState extends State<SignUpScreen> {
  TextEditingController fullName = new TextEditingController();
  TextEditingController emailAddress = new TextEditingController();
  TextEditingController password = new TextEditingController();
  TextEditingController confirmPassword = new TextEditingController();
  final _mobileNumber =  MaskedTextController(mask: '000-000-0000');
  Map userProfile;
  final facebookLogin = FacebookLogin();
  GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['email']);
  bool obscurePasswordText = true, obscureConfirmPasswordText = true;
  _loginWithFB() async {
    final result = await facebookLogin.logInWithReadPermissions(['email']);

    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final token = result.accessToken.token;
        final graphResponse = await http.get(
            'https://graph.facebook.com/v2.12/me?fields=name,picture,email&access_token=$token');
        final profile = JSON.jsonDecode(graphResponse.body);
        print(profile);
        setState(() {
          userProfile = profile;
          facebookLogin.logOut();
          Utility.showToastMessage(context, 'Welcome '+ userProfile['name']);
          Utility.setSharedPreference(MyStrings.FULL_NAME, userProfile['name']);
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeScreen()));
          _googleSignIn.signOut();
        });
        break;

      case FacebookLoginStatus.cancelledByUser:
        break;
      case FacebookLoginStatus.error:
        break;
    }
  }

  _googleLogin() async{
    try{
      await _googleSignIn.signIn();
      Utility.showToastMessage(context, 'Welcome '+_googleSignIn.currentUser.displayName);
      Utility.setSharedPreference(MyStrings.FULL_NAME, _googleSignIn.currentUser.displayName);
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeScreen()));
    } catch (err){
      print(err);
    }
  }

  @override
  Widget build(BuildContext context) {
    final node = FocusScope.of(context);
    return new Scaffold(
      backgroundColor: MyColors.colorWhite,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                child: Card(
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                  child: Container(
                      padding: EdgeInsets.fromLTRB(0, 50, 0, 20),
                      width: double.infinity,
                      height: 150,
                      child: Center(
                          child: Image.asset(Utility.getImage('food_konnekt_logo.png'), fit: BoxFit.fill,),
                      ),
                  ),
                ),
                decoration: new BoxDecoration(
                  boxShadow: [
                    new BoxShadow(
                      color: Colors.black12,
                      blurRadius: 21.1,
                    ),
                  ],
                ),
              ), // toolbar icon
              Container(
                margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                child: Text(MyStrings.TITLE_CREATE_ACCOUNT,
                    style: MyText.display1(context).copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                    ),
                ),
              ), // Title Create Account
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                child: Text(MyStrings.SUB_TITLE_SIGN_UP,
                    textAlign: TextAlign.center,
                    style: MyText.body2(context).copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.normal,
                    ),
                ),
              ), // Sub title sign up
              Container(
                margin: EdgeInsets.fromLTRB(50, 10, 50, 0),
                child: TextField(
                  controller: fullName,
                  maxLines: 1,
                  onEditingComplete: () => node.nextFocus(),
                  keyboardType: TextInputType.name,
                  style: MyText.body1(context).copyWith(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                  ),
                  decoration: InputDecoration(
                    labelText: MyStrings.FULL_NAME,
                    labelStyle: MyText.body1(context).copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.normal,
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.blueGrey[400], width: 1),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.blueGrey[400], width: 2),
                    ),
                  ),
                ),
              ), // TextField Name
              Container(
                margin: EdgeInsets.symmetric(horizontal: 50),
                child: TextField(
                  controller: _mobileNumber,
                  maxLines: 1,
                  onEditingComplete: () => node.nextFocus(),
                  keyboardType: TextInputType.number,
                  style: MyText.body1(context).copyWith(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                  ),
                  decoration: InputDecoration(
                    labelText: MyStrings.MOBILE_NUMBER,
                    labelStyle: MyText.body1(context).copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.normal,
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.blueGrey[400], width: 1),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.blueGrey[400], width: 2),
                    ),
                  ),
                ),
              ), // TextField Mobile Number
              Container(
                margin: EdgeInsets.symmetric(horizontal: 50),
                child: TextField(
                  controller: emailAddress,
                  maxLines: 1,
                  onEditingComplete: () => node.nextFocus(),
                  keyboardType: TextInputType.emailAddress,
                  style: MyText.body1(context).copyWith(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                  ),
                  decoration: InputDecoration(
                    labelText: MyStrings.EMAIL_ADDRESS,
                    labelStyle: MyText.body1(context).copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.normal,
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.blueGrey[400], width: 1),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.blueGrey[400], width: 2),
                    ),
                  ),
                ),
              ), // TextField Email address
              Container(
                margin: EdgeInsets.symmetric(horizontal: 50),
                child: Row(
                  children: [
                    Expanded(
                      flex: 6,
                      child: TextField(
                        controller: password,
                        maxLines: 1,
                        obscureText: obscurePasswordText,
                        onEditingComplete: () => node.nextFocus(),
                        keyboardType: TextInputType.visiblePassword,
                        style: MyText.body1(context).copyWith(
                          color: Colors.black,
                          fontWeight: FontWeight.normal,
                        ),
                        decoration: InputDecoration(
                          labelText: MyStrings.PASSWORD,
                          labelStyle: MyText.body1(context).copyWith(
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.blueGrey[400], width: 1),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.blueGrey[400], width: 2),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        width: 25,
                        height: 25,
                        child: GestureDetector(
                          onTap: (){
                            togglePassword();
                          },
                          child: Icon(obscurePasswordText? Icons.remove_red_eye: Icons.remove_red_eye_outlined),
                        ),
                      ),
                    ),
                  ],
                ),
              ), // TextField Password
              Container(
                margin: EdgeInsets.symmetric(horizontal: 50),
                child: Row(
                  children: [
                    Expanded(
                      flex: 6,
                      child: TextField(
                        controller: confirmPassword,
                        maxLines: 1,
                        obscureText: obscureConfirmPasswordText,
                        onEditingComplete: () => node.nextFocus(),
                        keyboardType: TextInputType.visiblePassword,
                        style: MyText.body1(context).copyWith(
                          color: Colors.black,
                          fontWeight: FontWeight.normal,
                        ),
                        decoration: InputDecoration(
                          labelText: MyStrings.CONFIRM_PASSWORD,
                          labelStyle: MyText.body1(context).copyWith(
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.blueGrey[400], width: 1),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.blueGrey[400], width: 2),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        width: 25,
                        height: 25,
                        child: GestureDetector(
                          onTap: (){
                            toggleConfirmPassword();
                          },
                          child: Icon(obscureConfirmPasswordText? Icons.remove_red_eye: Icons.remove_red_eye_outlined),
                        ),
                      ),
                    ),
                  ],
                ),
              ), // TextField Confirm Password
              Container(
                width: double.infinity,
                margin: EdgeInsets.symmetric(horizontal: 60),
                child: FlatButton(
                  color: MyColors.primary,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Text(
                    MyStrings.CREATE,
                    style: MyText.body2(context).copyWith(
                      color: MyColors.colorWhite,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onPressed: () {
                    if (fullName.text.toString().isEmpty) {
                      Utility.showToastMessage(
                          context, MyStrings.ENTER_YOUR_NAME);
                    } else if (_mobileNumber.text.toString().isEmpty) {
                      Utility.showToastMessage(
                          context, MyStrings.ENTER_YOUR_MOBILE_NUMBER);
                    } else if ((_mobileNumber.text.length < 9)) {
                      Utility.showToastMessage(
                          context, MyStrings.ENTER_VALID_MOBILE_NUMBER);
                    } else if (emailAddress.text.toString().isEmpty) {
                      Utility.showToastMessage(
                          context, MyStrings.ENTER_YOUR_EMAIL_ADDRESS);
                    } else if (!Utility.validateEmail(emailAddress.text.toString())) {
                      Utility.showToastMessage(
                          context, MyStrings.INVALID_EMAIL_ADDRESS);
                    } else if (password.text.toString().isEmpty) {
                      Utility.showToastMessage(
                          context, MyStrings.ENTER_YOUR_PASSWORD);
                    } else if (confirmPassword.text.toString().isEmpty) {
                      Utility.showToastMessage(
                          context, MyStrings.ENTER_YOUR_PASSWORD_AGAIN);
                    } else if (password.text.toString().trim() != confirmPassword.text.toString().trim()) {
                      Utility.showToastMessage(
                          context, MyStrings.PASSWORD_DO_NOT_MATCH);
                    } else {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) => HomeScreen()));
                    }
                  },
                ),
              ), // FlatButton Sign up
              Container(
                margin: EdgeInsets.symmetric(vertical: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10.0),
                      child: Container(
                        height: 1.0,
                        width: 100.0,
                        color: Colors.black,
                      ),
                    ),
                    Text(
                      MyStrings.OR,
                      style: MyText.body2(context).copyWith(
                        color: Colors.black,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10.0),
                      child: Container(
                        height: 1.0,
                        width: 100.0,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ), // divider design ---- or ----
              Container(
                margin: EdgeInsets.fromLTRB(60, 5, 60, 20),
                child: Text(MyStrings.CREATE_ACCOUNT_USING_SOCIAL_MEDIA,
                    textAlign: TextAlign.center,
                    style: MyText.body2(context).copyWith(color: Colors.black, fontWeight: FontWeight.normal,),
                ),
              ), // Sub title Create account using social media
              Container(
                width: double.infinity,
                margin: EdgeInsets.symmetric(horizontal: 60),
                child: FlatButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                  color: Colors.indigo,
                  child: Text(
                    MyStrings.JOIN_VIA_FACEBOOK,
                    style: MyText.body2(context).copyWith(color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    _loginWithFB();
                  },
                ),
              ), // Join via facebook
              Container(
                width: double.infinity,
                margin: EdgeInsets.symmetric(horizontal: 60),
                child: FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  color: Colors.redAccent,
                  child: Text(
                    MyStrings.JOIN_VIA_GOOGLE,
                    style: MyText.body2(context).copyWith(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onPressed: () {
                    _googleLogin();                  // Google login
                  },
                ),
              ), // Join via google
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                child: GestureDetector(
                  child: RichText(
                    text: TextSpan(
                        text: MyStrings.ALREADY_A_MEMBER+ '  ',
                        style: MyText.body2(context).copyWith(color: Colors.black, fontWeight: FontWeight.normal),
                        children: [
                          TextSpan(
                            text: MyStrings.SIGN_IN,
                            style: MyText.body2(context).copyWith(
                              color: MyColors.primary,
                              fontWeight: FontWeight.normal,
                              decoration: TextDecoration.underline,
                            ),
                          ),
                        ]
                    ),
                  ),
                  onTap: (){
                    if (Navigator.canPop(context)) {
                      Navigator.pop(context);
                    } else {
                      SystemNavigator.pop();
                    }
                  } ,
                ),
              ), // Row already a member> sign in
            ],
          ),
        ),
      ),
    );
  }

  void togglePassword() {
    setState(() {
      obscurePasswordText = !obscurePasswordText;
    });
  }

  void toggleConfirmPassword() {
    setState(() {
      obscureConfirmPasswordText = !obscureConfirmPasswordText;
    });
  }

}
