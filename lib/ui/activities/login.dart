import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:mkonnekt_kitchen/data/my_colors.dart';
import 'package:mkonnekt_kitchen/data/my_strings.dart';
import 'package:mkonnekt_kitchen/data/my_text.dart';
import 'package:mkonnekt_kitchen/data/utills.dart';
import 'package:mkonnekt_kitchen/ui/activities/signup.dart';
import 'home.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> {
  final _mobileFormatter = MaskedTextController(mask: '000-000-0000');
  TextEditingController txvPassword = new TextEditingController();
  bool obscurePasswordText = true;
  Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;

  void togglePassword() {
    setState(() {
      obscurePasswordText = !obscurePasswordText;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      _currentPosition = Utility.getCurrentLocation(geolocator);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: new Scaffold(
        backgroundColor: MyColors.colorWhite,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  child: Card(
                    margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                    child: Container(
                        padding: EdgeInsets.fromLTRB(0, 50, 0, 20),
                        width: double.infinity,
                        height: 150,
                        child: Center(
                            child: Image.asset(
                          Utility.getImage('food_konnekt_logo.png'),
                          fit: BoxFit.fill,
                        ))),
                  ),
                  decoration: new BoxDecoration(
                    boxShadow: [
                      new BoxShadow(
                        color: Colors.black12,
                        blurRadius: 21.1,
                      ),
                    ],
                  ),
                ), // toolbar icon
                Container(
                  margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                  child: Text(MyStrings.TITLE_LOGIN,
                      style: MyText.display1(context).copyWith(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      )),
                ), // Title Glad to see you
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  child: Text(MyStrings.SUB_TITLE_TITLE_LOGIN,
                      style: MyText.body2(context).copyWith(
                        color: Colors.black,
                        fontWeight: FontWeight.normal,
                      )),
                ), // Sub title sign in
                Container(
                  margin: EdgeInsets.fromLTRB(50, 50, 50, 0),
                  child: TextField(
                    controller: _mobileFormatter,
                    maxLines: 1,
                    keyboardType: TextInputType.number,
                    style: MyText.body1(context).copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.normal,
                    ),
                    decoration: InputDecoration(
                      labelText: MyStrings.ENTER_YOUR_MOBILE_NUMBER,
                      labelStyle: MyText.body1(context).copyWith(
                        color: Colors.black,
                        fontWeight: FontWeight.normal,
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.blueGrey[400], width: 1),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.blueGrey[400], width: 2),
                      ),
                    ),
                  ),
                ), // Textfield mobile
                Container(
                  margin: EdgeInsets.fromLTRB(50, 10, 50, 50),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 6 ,
                        child: TextField(
                          controller: txvPassword,
                          maxLines: 1,
                          keyboardType: TextInputType.text,
                          obscureText: obscurePasswordText,
                          enableSuggestions: false,
                          autocorrect: false,
                          style: MyText.body1(context).copyWith(
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                          ),
                          decoration: InputDecoration(
                            labelText: MyStrings.PASSWORD,
                            labelStyle: MyText.body1(context).copyWith(
                              color: Colors.black,
                              fontWeight: FontWeight.normal,
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.blueGrey[400], width: 1),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.blueGrey[400], width: 2),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          width: 25,
                          height: 25,
                          child: GestureDetector(
                            onTap: (){
                              togglePassword();
                            },
                            child: Icon(obscurePasswordText? Icons.remove_red_eye: Icons.remove_red_eye_outlined),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // Textfield password
                Container(
                  width: double.infinity,
                  margin: EdgeInsets.fromLTRB(50, 0, 50, 0),
                  child: FlatButton(
                    color: MyColors.primary,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Text(
                      MyStrings.SIGN_IN,
                      style: MyText.body2(context).copyWith(
                        color: MyColors.colorWhite,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onPressed: () {
                      if (_mobileFormatter.text.toString().isEmpty) {
                        Utility.showToastMessage(
                            context, MyStrings.ENTER_YOUR_MOBILE_NUMBER);
                      } else if ((_mobileFormatter.text.length < 9)) {
                        Utility.showToastMessage(
                            context, MyStrings.ENTER_VALID_MOBILE_NUMBER);
                      } else if (txvPassword.text.toString().isEmpty) {
                        Utility.showToastMessage(
                            context, MyStrings.ENTER_YOUR_PASSWORD);
                      } else if ((txvPassword.text.length < 4)) {
                        Utility.showToastMessage(
                            context, MyStrings.ENTER_VALID_PASSWORD);
                      } else {
                        Utility.setSharedPreference(
                            MyStrings.TOKEN,
                            _mobileFormatter.text
                                .toString()
                                .trim()
                                .replaceAll('-', ''));
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HomeScreen()));
                      }
                    },
                  ),
                ), // FlatButton Sign in
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10),
                  child: GestureDetector(
                    child: RichText(
                      text: TextSpan(
                          text: MyStrings.NOT_A_MEMBER+ "  ",
                          style: MyText.body2(context).copyWith(
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                          ),
                          children: [
                            TextSpan(
                              text: MyStrings.SIGN_UP,
                              style: MyText.body2(context).copyWith(
                                color: MyColors.primary,
                                fontWeight: FontWeight.normal,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                          ]
                      ),
                    ),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => SignUpScreen()));
                    },
                  ),
                ), // Row not a member> sign up
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() async {
    return (await showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            content: new Text(MyStrings.DO_YOU_WANT_TO_EXIT),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text(MyStrings.NO),
              ),
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: new Text(MyStrings.YES),
              ),
            ],
          ),
        )) ??
        false;
  }
}
