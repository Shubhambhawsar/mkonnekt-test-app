import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mkonnekt_kitchen/data/my_colors.dart';
import 'package:mkonnekt_kitchen/ui/fragments/cart_items_fragment.dart';
import 'package:mkonnekt_kitchen/ui/fragments/check_fragment.dart';

class MyCartScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return new MyCartScreenState();
  }
}

class MyCartScreenState extends State<MyCartScreen> with TickerProviderStateMixin {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          backgroundColor: MyColors.colorWhite,
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(kToolbarHeight),
            child: Container(
              color: MyColors.primary,
              child: TabBar(
                indicatorColor: Colors.white,
                tabs: [
                  Padding(padding:EdgeInsets.all(10.0), child: Text('Cart Items', style: TextStyle(color: Colors.white))),
                  Text('Checkout', style: TextStyle(color: Colors.white)),
                ],
              ),
            ),
          ),
          body: TabBarView(
            children: [
              CartItemList(),
              CheckOutScreen(),
            ],
          ),
        ),
      ),
    );
  }
}
