import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:mkonnekt_kitchen/data/my_colors.dart';
import 'package:mkonnekt_kitchen/data/my_strings.dart';
import 'package:mkonnekt_kitchen/ui/activities/login.dart';
import 'package:mkonnekt_kitchen/ui/activities/side_drawer.dart';
import 'package:mkonnekt_kitchen/ui/fragments/games_fragment.dart';
import 'package:mkonnekt_kitchen/ui/fragments/home_fragment.dart';
import 'package:mkonnekt_kitchen/ui/fragments/menu_items_fragment.dart';
import 'package:mkonnekt_kitchen/ui/fragments/our_location_fragment.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(HomeScreen());
}

class HomeScreen extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _currentIndex = 0;
  PageController viewpagerController = new PageController(initialPage: 0);
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: _onBackPressed,
        child: new Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.white,
          drawer: navigationDrawer(onDrawerTapped),
          body: Column(
            children: [
              GestureDetector(
                child:Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    margin: EdgeInsets.fromLTRB(10.0, 10.0, 0, 0),
                    child: SizedBox( width: 30.0, height: 30.0,
                    child: Icon(Icons.apps, color: MyColors.primary,),),
                  ),
                ) ,
                onTap: () {
                  openDrawer();
                },
              ),
              Expanded(
                child: PageView(
                  controller: viewpagerController,
                  allowImplicitScrolling: true,
                  physics: new NeverScrollableScrollPhysics(),
                  children: [
                    OffersList(),
                    OurLocation(),
                    MenuItemScreen(),
                    GamesScreen(),
                  ],
                ),
              ),
            ],
          ),
          bottomNavigationBar: BottomNavigationBar(
            items: [
              BottomNavigationBarItem(
                icon: new Icon(Icons.home),
                title: Text(MyStrings.HOME),
              ),
              BottomNavigationBarItem(
                icon: new Icon(Icons.location_on),
                title: Text(MyStrings.LOCATIONS),
              ),
            BottomNavigationBarItem(
                icon: new Icon(Icons.wine_bar),
                title: Text(MyStrings.ORDER),
              ),
            BottomNavigationBarItem(
                icon: new Icon(Icons.shield),
                title: Text(MyStrings.REWARDS),
              ),
            BottomNavigationBarItem(
                icon: new Icon(Icons.videogame_asset_sharp),
                title: Text(MyStrings.GAME),
              ),
            ],
            selectedItemColor: MyColors.primary,
            unselectedItemColor: Colors.black12,
            onTap: onTabTapped,
            currentIndex: _currentIndex,
          ),
        ),
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;

      if (index == 0)
        viewpagerController.jumpToPage(0);
      else if (index == 1)
        viewpagerController.jumpToPage(1);
      else if (index == 2)
        viewpagerController.jumpToPage(2);
      else if (index == 4)
        viewpagerController.jumpToPage(3);
    });
    closeDrawer();
  }

  void onDrawerTapped(int index) {
    if (index == 0)
      onTabTapped(0);
    else if (index == 1)
      onTabTapped(1);
    else if (index == 2)
      onTabTapped(2);
    else if (index == 4)
      onTabTapped(4);
    else if(index == 11)
      askForLogoutDialog(context);
  }

  closeDrawer() async {
    if (_scaffoldKey.currentState.isDrawerOpen) {
      _scaffoldKey.currentState.openEndDrawer();
    }
  }

  openDrawer() async {
    _scaffoldKey.currentState.openDrawer();
  }

  void logout(BuildContext context) async{
    SharedPreferences preferences =  await SharedPreferences.getInstance();
    preferences.clear();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      Navigator.of(context).push(MaterialPageRoute(builder: (context) => LoginScreen()));
    });
  }

  Future<bool> askForLogoutDialog(BuildContext context) async {
    return (await showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        content: new Text(MyStrings.DO_YOU_WANT_TO_LOGOUT),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text(MyStrings.NO),
          ),
          new FlatButton(
            onPressed: () =>       logout(context),
            child: new Text(MyStrings.YES),
          ),
        ],
      ),
    )) ??
        false;
  }

  Future<bool> _onBackPressed() async {
    return (await showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        content: new Text(MyStrings.DO_YOU_WANT_TO_EXIT),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text(MyStrings.NO),
          ),
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: new Text(MyStrings.YES),
          ),
        ],
      ),
    )) ??
        false;
  }
}