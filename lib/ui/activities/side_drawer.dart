import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mkonnekt_kitchen/data/my_colors.dart';
import 'package:mkonnekt_kitchen/data/my_strings.dart';
import 'package:mkonnekt_kitchen/data/utills.dart';
import 'package:shared_preferences/shared_preferences.dart';

class navigationDrawer extends StatelessWidget {
  navigationDrawer(this.onDrawerTapped);
  final Function onDrawerTapped;
  String userName = "";

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          createDrawerHeader(),
          createDrawerBodyItem(icon: Icons.home, text: 'Home', onTap: () => onDrawerTapped(0)),
          createDrawerBodyItem(icon: Icons.account_circle, text: 'Profile'),
          createDrawerBodyItem(icon: Icons.event_note, text: 'Orders', onTap: () => onDrawerTapped(2)),
          createDrawerBodyItem(icon: Icons.videogame_asset_sharp, text: 'Games', onTap: () => onDrawerTapped(4)),
          Divider(),
          createDrawerBodyItem(icon: Icons.notifications_active, text: 'Notifications'),
          createDrawerBodyItem(icon: Icons.contact_phone, text: 'Contact Info'),
          createDrawerBodyItem(icon: Icons.settings_power, text: 'Log out', onTap:() => onDrawerTapped(11)),
          ListTile(
            title: Text('App version 1.0.0'),
            onTap: () {},
          ),
        ],
      ),
    );
  }

  Widget createDrawerHeader() {
    initialise();
    return DrawerHeader(
        margin: EdgeInsets.zero,
        padding: EdgeInsets.zero,
        decoration: BoxDecoration(color: MyColors.primary),
        child: Stack(children: <Widget>[
          Positioned(
              bottom: 12.0,
              left: 16.0,
              child: Text(userName,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold))),
        ]));
  }

  Widget createDrawerBodyItem({IconData icon, String text, GestureTapCallback onTap}) {
    return ListTile(
      title: Row(
        children: <Widget>[
          SizedBox(
            width: 20.0, height: 20.0,
              child: Icon(icon)),
          Padding(
            padding: EdgeInsets.only(left: 8.0),
            child: Text(text, style: TextStyle(fontSize: 14.0, color: Colors.black),),
          )
        ],
      ),
      onTap: onTap,
    );
  }

  Future<void> initialise() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userName  = prefs.getString(MyStrings.FULL_NAME) ?? null;
    print('Name' + userName);
  }
}
