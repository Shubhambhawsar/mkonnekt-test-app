class MyStrings {
/*------------ API URLs------------*/
  static const String BASE_URL = "https://www.foodkonnekt.com/";
  static const String SERVER = "admin/";
  static const String GET_MENU_CATEGORIES_URL = BASE_URL+ SERVER+  "getMerchantMenuCategories";
  static const String GET_MENU_ITEM_BY_CATEGORIES_URL = BASE_URL+ SERVER+  "getMenuItemsByCategory";
  static const String TOKEN = "token";

  /*  --------------- Login screen ------------       */
  static const String TITLE_LOGIN = "Glad to see you!";
  static const String SUB_TITLE_TITLE_LOGIN = "Sign in to your account and get started";
  static const String PASSWORD = 'Password';
  static const String ENTER_YOUR_PASSWORD = "Enter your password";
  static const String ENTER_YOUR_PASSWORD_AGAIN = "Enter your password again";
  static const String PASSWORD_DO_NOT_MATCH = "Password do not match";
  static const String ENTER_VALID_PASSWORD = "Enter a valid password";
  static const String ENTER_YOUR_MOBILE_NUMBER = "Enter your mobile number";
  static const String ENTER_VALID_MOBILE_NUMBER = "Enter a valid mobile number";
  static const String ENTER_YOUR_EMAIL_ADDRESS = "Enter your email address";
  static const String INVALID_EMAIL_ADDRESS = "Enter a valid email address";
  static const String SIGN_IN = "Sign In";
  static const String NOT_A_MEMBER = "Not a Member yet?";
  static const String ALREADY_A_MEMBER = "Already a Member?";
  static const String JOIN_US = "Join Us";
  static const String SIGN_UP = "Sign Up";
  static const String NO = "No";
  static const String YES = "Yes";
  static const String DO_YOU_WANT_TO_EXIT = "Do you want to exit?";
  static const String DO_YOU_WANT_TO_LOGOUT = "Do you want to logout?";

  /* --------------- SignUp Screen -----------*/
  static const String TITLE_CREATE_ACCOUNT = "Create Account";
  static const String SUB_TITLE_SIGN_UP = "Complete your account by entering\nyour details below";
  static const String FULL_NAME = "Full Name";
  static const String ENTER_YOUR_NAME = "Enter your name";
  static const String MOBILE_NUMBER = "Mobile Number";
  static const String CREATE = "Create";
  static const String OR = "Or";
  static const String CREATE_ACCOUNT_USING_SOCIAL_MEDIA = "Create account using social medias";
  static const String JOIN_VIA_FACEBOOK = "Join via Facebook";
  static const String JOIN_VIA_GOOGLE = "Join via Google";
  static const String EMAIL_ADDRESS = "Email Addredss";
  static const String CONFIRM_PASSWORD = "Confirm Password";

  /*---------------- Home Screen--------------*/
  static const String HOME = "Home";
  static const String LOCATIONS = "Locations";
  static const String ORDER = "Order";
  static const String REWARDS = "Rewards";
  static const String GAME = "Game";
  static const String KNOW_MORE = "Know More";

  /*-------------- OurLocation Screen  --------------*/
  static const String OUR_LOCATION = "Our Location";
  static const String CALL_US = "Call Us";
  static const String FIND_US = "Find Us";
  static const String CONNECT_WITH_US = "Connect with us";
  static const String WORKING_HOURS = "Working Hours";
  static const String FIND_YOUR_NEAR_BY_STORE = "Find your near by store";

  /*------------- Item List Screen -----------------*/
  static const String ADD = 'Add';
  static const String ITEM_NAME = 'Item Name';
  static const String POPULAR = 'Most Popular';
  static const String VEG = 'Veg';
  static const String NON_VEG = 'Non Veg';
  static const String SEARCH_BY_NAME_ADDRESS = "Search by name, address";

  static const String lorem_ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur ipsum in placerat molestie.  Fusce quis mauris a enim sollicitudin"
      "\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur ipsum in placerat molestie.  Fusce quis mauris a enim sollicitudin";

  static const String middle_lorem_ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur ipsum in placerat molestie.  Fusce quis mauris a enim sollicitudin";

  static const String short_lorem_ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";

  static const String medium_lorem_ipsum = "Quisque imperdiet nunc at massa dictum volutpat. Etiam id orci ipsum. Integer id ex dignissim";

  static const String long_lorem_ipsum = "Duis tellus metus, elementum a lectus id, aliquet interdum mauris. Nam bibendum efficitur sollicitudin. Proin eleifend libero velit, "
      "nec fringilla dolor finibus quis. nMorbi eu libero pellentesque, rutrum metus quis, blandit est. Fusce bibendum accumsan nisi vulputate feugiat. In fermentum laoreet euismod. Praesent sem nisl, "
      "facilisis eget odio at, rhoncus scelerisque ipsum. Nulla orci dui, dignissim a risus ut, lobortis porttitor velit."
      "\n\nNulla id lectus metus. Maecenas a lorem in odio auctor facilisis non vitae nunc. Sed malesuada volutpat massa. Praesent sit amet lacinia augue, mollis tempor dolor.";

  static const String long_lorem_ipsum_2 = "Vivamus porttitor, erat at aliquam mollis, risus ligula suscipit metus, id semper nisi dui lacinia leo. Praesent at feugiat sem. Vivamus consectetur arcu sit amet metus efficitur dapibus. Sed metus ligula, efficitur quis finibus nec, pellentesque nec nulla. Nunc nec magna iaculis turpis vehicula condimentum id lobortis lectus."
    "\n\nQuisque augue diam, convallis nec mollis in, placerat nec elit. Aliquam non erat tristique, consequat tortor ultricies, sodales erat. Aliquam quis enim eu nulla facilisis sollicitudin eu mollis mauris. Donec ornare urna non libero volutpat tempus. Mauris elementum egestas ex, a laoreet urna. Ut a magna mattis, sodales massa a, blandit justo";

  static const String invoice_address = "Terry M Smith\n(904) 246-1297\n207 Cherry St, Neptune Beach, FL, 32266";

  static const String  very_long_lorem_ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dolor risus, aliquet a erat quis, viverra molestie orci. Suspendisse vehicula porta libero. Nam tincidunt nulla ipsum, vel iaculis risus scelerisque sed. Phasellus venenatis, enim vel placerat blandit, leo eros bibendum erat, at auctor mauris diam ac risus. Aenean sit amet congue neque, sit amet condimentum elit. Fusce lacinia massa vel nisl scelerisque, in scelerisque dolor elementum. Vivamus leo enim, congue dictum congue vitae, porttitor id purus. Sed eu ultricies erat. Morbi hendrerit, mi ac volutpat commodo, magna turpis pretium nibh, at fringilla eros lorem quis tellus. Praesent porttitor purus nibh, ac vestibulum massa fringilla vel. Pellentesque dapibus nulla quis luctus dictum. In scelerisque ut ex sed facilisis. Nunc eu finibus nulla, ut hendrerit sem. Suspendisse accumsan risus vel diam fringilla iaculis."
      "\n\nQuisque facilisis finibus aliquam. Sed mattis, dui fringilla ornare finibus, orci odio blandit risus, ut maximus dui odio non leo. Donec laoreet lacus nisi, a mollis velit suscipit ac. Quisque faucibus ut nisl in congue. Nam sagittis consectetur tempus. Suspendisse eget posuere dui. Donec ultricies velit ex, vel cursus odio sollicitudin in. Nullam faucibus auctor ligula, eu rhoncus dui facilisis a. Curabitur vel feugiat est. Quisque congue at enim eleifend ultrices. Proin dapibus risus lorem, nec condimentum nunc consequat vitae. Quisque sapien lorem, vestibulum vitae justo eget, fringilla eleifend nisi."
      "\n\nMauris ultricies augue sit amet est sollicitudin laoreet. Fusce ut congue felis. Fusce dictum tristique elit nec iaculis. Mauris sodales tempus fringilla. Fusce nec nunc tempus, tempor sapien sit amet, ultricies erat. Mauris ultrices ac lorem ultrices facilisis. Nam pharetra, nisi a imperdiet interdum, nunc justo ultricies nisl, a laoreet massa dui vel enim. Pellentesque et magna ac tellus ullamcorper malesuada a ac nisl. Curabitur id est et neque convallis accumsan. Aliquam eleifend varius massa. Curabitur eu finibus tortor."
      "\n\nVivamus aliquam nisl rutrum orci volutpat porta in ornare est. Quisque ligula ipsum, vulputate id aliquam vitae, semper vitae orci. Nam non pulvinar ligula, eget tincidunt ante. Quisque sapien massa, varius tempus pretium id, vehicula at magna. Ut ut ultrices augue. Nam nec ullamcorper tellus, at finibus lectus. Fusce vel iaculis mauris, id porta nulla. Aenean ac mollis sapien. Morbi imperdiet augue tempus nulla luctus, sit amet feugiat purus elementum."
      "\n\nIn sit amet rutrum diam. Vivamus laoreet aliquam ipsum eget pretium. Mauris sagittis non elit quis fermentum. Aenean at diam nec tortor maximus rutrum. Sed nec nulla volutpat quam suscipit varius non at erat. Pellentesque non euismod diam, nec dapibus quam. Aliquam et posuere massa. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam eu nibh sed diam posuere tincidunt et non felis. Aliquam sodales facilisis tortor, at maximus ante varius quis."
      "\n\nSuspendisse ornare est ac auctor pulvinar. Nam in venenatis risus. In facilisis tristique mollis. Curabitur tempus ipsum eget ipsum pharetra ornare. Quisque imperdiet nunc at massa dictum volutpat. Etiam id orci ipsum. Integer id ex dignissim est blandit sollicitudin non ut felis. Mauris nec mattis lacus. Cras consequat sapien a nisl faucibus, id consequat velit aliquet. Donec tincidunt quam elit, et scelerisque ipsum aliquet ac. Suspendisse lectus enim, auctor non justo et, vehicula consectetur nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fermentum ipsum vitae ligula interdum vulputate. Aenean bibendum molestie pharetra. Praesent aliquam, sapien ac rutrum rhoncus, enim mauris lacinia ligula, vitae fermentum purus lacus nec ex. Donec vel dolor pulvinar, dignissim nunc id, tempus risus."
      "\n\nPhasellus blandit, leo ut semper vulputate, dui est fringilla purus, eu ullamcorper leo quam ut odio. Nunc tincidunt est eros, a dapibus massa hendrerit in. Aenean pellentesque ante eu justo pulvinar, eget sodales diam auctor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent vulputate nunc ut lorem viverra, ac condimentum sem vehicula. Etiam in dui quam. Aenean tincidunt porttitor dignissim. Aenean id est erat. Praesent tempus est ullamcorper tincidunt ultricies. Mauris ac velit lorem. Maecenas quis dui vel neque aliquet mattis aliquet et ante."
      "\n\nVivamus porttitor, erat at aliquam mollis, risus ligula suscipit metus, id semper nisi dui lacinia leo. Praesent at feugiat sem. Vivamus consectetur arcu sit amet metus efficitur dapibus. Sed metus ligula, efficitur quis finibus nec, pellentesque nec nulla. Nunc nec magna iaculis turpis vehicula condimentum id lobortis lectus. Etiam tempus nisi arcu, in condimentum felis vestibulum vitae. Donec fermentum dolor consequat nulla dignissim, non tempus leo viverra. Aenean magna magna, ultricies ut rutrum et, malesuada non nibh. Nam ligula erat, elementum sit amet justo eget, venenatis hendrerit nibh. In ac ipsum nunc. Nullam purus dolor, rhoncus eu justo id, vulputate ultrices nunc. Nam et risus velit. Fusce aliquam blandit urna quis pulvinar. Donec luctus tincidunt ipsum eu condimentum. Etiam porttitor dui in pulvinar vulputate."
      "\n\nQuisque augue diam, convallis nec mollis in, placerat nec elit. Aliquam non erat tristique, consequat tortor ultricies, sodales erat. Aliquam quis enim eu nulla facilisis sollicitudin eu mollis mauris. Donec ornare urna non libero volutpat tempus. Mauris elementum egestas ex, a laoreet urna. Ut a magna mattis, sodales massa a, blandit justo. Nunc placerat ante quis lacus dictum dictum vitae eu nisi. Fusce quis est sodales justo sollicitudin mollis. Suspendisse posuere, augue a ultricies lacinia, tortor tortor faucibus purus, pulvinar suscipit erat orci varius arcu. Phasellus id nisl vitae eros dignissim consectetur. Ut ac ex ac arcu vestibulum sagittis sit amet a massa."
      "\n\nSuspendisse elit libero, placerat vel ipsum vitae, tristique faucibus nulla. Aliquam ac elit porttitor, vestibulum turpis nec, bibendum dui. Vivamus quam orci, consequat in scelerisque id, dignissim vel purus. Proin rhoncus ante eu nisi dictum lacinia. Proin congue tempor nunc, eu semper elit tincidunt ut. Duis vehicula purus at lorem sollicitudin porttitor. Nulla facilisi. Donec non dapibus neque. Donec neque massa, sodales vitae eros nec, lacinia vestibulum neque. Nullam venenatis vitae tortor sed fermentum. Aenean massa enim, placerat eu ligula quis, tincidunt rutrum purus. Ut tincidunt egestas varius.";

  static const String gdpr_privacy_policy = "By using this App, you agree to the Terms-Conditions "
      "Cookies-Policy and Privacy-Policy and consent to having your personal data, behavior transferred and processed outside EU.";

  // My Account keys
  static String STRIPE_PUBLISHED_KEY = "pk_test_51Hx6IFKQYT7LqT9fih6UTfq8TcRb4xF7r6CDDpubdhKlnkKA8H15cVV8LlmocbrpctHz5K6MDznFMTBZhduWIbbj003RulEmHg";
  static String STRIPE_SECRET_KEY = "sk_test_51Hx6IFKQYT7LqT9fPnJP8hxepOAUyimEvORp74aDd0lQb4rmAdNnXFVw9OVr3p2yxfnPygZDCx5uQ44PwMKIcVy200JgrfvdgR";



  // mKonnekt Account keys
  // static String STRIPE_PUBLISHED_KEY = "pk_test_51H5mmFGopA6QW2epKJdwF5eVmBnCxoiMPlsZDtarYPLN7sAMc9VzooAwwdtV9mpWG43jd9nncvUyqTSD6l5QaS7T000J2ofSfS";
  // static String STRIPE_SECRET_KEY = "sk_test_51H5mmFGopA6QW2epHg0WzvHsOtBReNe1u5P7ux4tueqQKVprZdiUFqf1nUmu9j1rRbhFBZjOpVmYe1wTN7mVVarr00ofP3Boey";

}