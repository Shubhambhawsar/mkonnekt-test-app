import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:mkonnekt_kitchen/data/my_strings.dart';
import 'model/models.dart';
import 'model/response/category_list.dart';
import 'model/response/menuItems.dart';

class ApiClient {

  static Future<CategoryList> getMenuCategories(String merchantId) async{
    var categoryList;
    var client = http.Client();
    String url = MyStrings.GET_MENU_CATEGORIES_URL + '?merchantUId='+merchantId;
    print(url);
    try {
      var response = await client.get(url);
      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {
          var jsonString = response.body;
          var jsonMap = json.decode(jsonString);
          categoryList = CategoryList.fromJson(jsonMap);
        }
      }
    } catch (Exception){
      return categoryList;
    }
    return categoryList;
  }

  static Future<MenuItemsList> getItemsByCategory(String categoryId, String merchantId) async{
    var menuItemList;
    var client = http.Client();
    String url =   MyStrings.GET_MENU_ITEM_BY_CATEGORIES_URL+'?categoryId=' +categoryId+'&merchantId='+merchantId;
    print("Get Menu Item Url : "+ url);
    try {
      var response = await client.get(url);
      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {
          var jsonString = response.body;
          var jsonMap = json.decode(jsonString);
          menuItemList =  MenuItemsList.fromJson(jsonMap);
        }
      }
    } catch (Exception){
      return menuItemList;
    }
    return menuItemList;
  }
}