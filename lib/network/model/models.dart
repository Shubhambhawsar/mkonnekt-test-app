class Category {
  Category({
    this.id,
    this.name,
    this.sortOrder,
    this.posCategoryId,
    this.itemStatus,
    this.merchant,
    this.items,
    this.pizzaSizes,
    this.categoryTimings,
    this.allowCategoryTimings,
    this.categoryImage,
    this.itemCount,
    this.days,
    this.startTime,
    this.endTime,
    this.categoryCountFoeExcelSheet,
    this.isPizza,
  });

  int id;
  String name;
  dynamic sortOrder;
  dynamic posCategoryId;
  dynamic itemStatus;
  dynamic merchant;
  dynamic items;
  dynamic pizzaSizes;
  dynamic categoryTimings;
  int allowCategoryTimings;
  dynamic categoryImage;
  dynamic itemCount;
  dynamic days;
  dynamic startTime;
  dynamic endTime;
  dynamic categoryCountFoeExcelSheet;
  int isPizza;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: json["id"],
    name: json["name"],
    sortOrder: json["sortOrder"],
    posCategoryId: json["posCategoryId"],
    itemStatus: json["itemStatus"],
    merchant: json["merchant"],
    items: json["items"],
    pizzaSizes: json["pizzaSizes"],
    categoryTimings: json["categoryTimings"],
    allowCategoryTimings: json["allowCategoryTimings"],
    categoryImage: json["categoryImage"],
    itemCount: json["itemCount"],
    days: json["days"],
    startTime: json["startTime"],
    endTime: json["endTime"],
    categoryCountFoeExcelSheet: json["categoryCountFoeExcelSheet"],
    isPizza: json["isPizza"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "sortOrder": sortOrder,
    "posCategoryId": posCategoryId,
    "itemStatus": itemStatus,
    "merchant": merchant,
    "items": items,
    "pizzaSizes": pizzaSizes,
    "categoryTimings": categoryTimings,
    "allowCategoryTimings": allowCategoryTimings,
    "categoryImage": categoryImage,
    "itemCount": itemCount,
    "days": days,
    "startTime": startTime,
    "endTime": endTime,
    "categoryCountFoeExcelSheet": categoryCountFoeExcelSheet,
    "isPizza": isPizza,
  };
}


class Notif {

  int id;
  String title;
  String content;
  String type;
  String image;
  String link;

  // extra attribute
  int createdAt;
  bool isRead = false;

  Notif(this.title, this.content){
    this.type = "NORMAL";
    this.createdAt = DateTime.now().millisecondsSinceEpoch;
    this.id = this.createdAt;
  }

  Notif.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.title = json['title'];
    this.content = json['content'];
    this.type = json['type'];
    this.image = json['image'];
    this.link = json['link'];
    this.createdAt = json['createdAt'];
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'title': title,
    'content': content,
    'type': type,
    'image': image,
    'link': link,
    'createdAt': createdAt,
  };

}