import 'dart:convert';

import '../models.dart';

MenuItemsList welcomeFromJson(String str) => MenuItemsList.fromJson(json.decode(str));
String welcomeToJson(MenuItemsList data) => json.encode(data.toJson());

class MenuItemsList {
  MenuItemsList({
    this.status,
    this.message,
    this.category,
    this.menuItems,
  });

  int status;
  String message;
  Category category;
  List<MenuItem> menuItems;

  factory MenuItemsList.fromJson(Map<String, dynamic> json) => MenuItemsList(
    status: json["status"],
    message: json["message"],
    category: Category.fromJson(json["category"]),
    menuItems: List<MenuItem>.from(json["menuItems"].map((x) => MenuItem.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "category": category.toJson(),
    "menuItems": List<dynamic>.from(menuItems.map((x) => x.toJson())),
  };
}

class Merchant {
  Merchant({
    this.id,
    this.posMerchantId,
    this.name,
    this.employeePosId,
    this.modileDeviceId,
    this.isFbAppPublished,
    this.modieImeiCode,
    this.allowAuxTax,
    this.fBTabIds,
    this.allowReOrder,
    this.allowFutureOrder,
    this.activeCustomerFeedback,
    this.futureDaysAhead,
    this.createdDate,
    this.updatedDate,
    this.phoneNumber,
    this.address,
    this.action,
    this.totalOrders,
    this.cashOrder,
    this.creditOrder,
    this.pickup,
    this.delivery,
    this.totalOrdersPrice,
    this.status,
    this.storeId,
    this.subscription,
    this.items,
    this.addresses,
    this.paymentModes,
    this.openingClosingDays,
    this.vouchars,
    this.orderTypes,
    this.merchantSubscriptions,
    this.modifiers,
    this.modifierGroups,
    this.orderRs,
    this.taxRates,
    this.merchantLogin,
    this.socialMediaLinks,
    this.timeZone,
    this.owner,
    this.merchantLogo,
    this.accessToken,
    this.isInstall,
    this.merchantUid,
    this.allowMultipleKoupon,
    this.allowMultiPay,
    this.website,
    this.birthdayKouponUrl,
    this.mailchimpApiKey,
    this.allowDeliveryTiming,
    this.couponCode,
    this.emailId,
    this.deliverystatus,
    this.tipsForPickup,
    this.tipsForDilevery,
    this.activeTipsForPickup,
    this.activeTipForDilevery,
    this.ordersList,
    this.toMail,
    this.partnerName,
  });

  int id;
  String posMerchantId;
  MerchantName name;
  dynamic employeePosId;
  String modileDeviceId;
  bool isFbAppPublished;
  String modieImeiCode;
  int allowAuxTax;
  dynamic fBTabIds;
  bool allowReOrder;
  int allowFutureOrder;
  int activeCustomerFeedback;
  int futureDaysAhead;
  AtedDate createdDate;
  AtedDate updatedDate;
  PhoneNumber phoneNumber;
  dynamic address;
  dynamic action;
  dynamic totalOrders;
  dynamic cashOrder;
  dynamic creditOrder;
  dynamic pickup;
  dynamic delivery;
  dynamic totalOrdersPrice;
  dynamic status;
  dynamic storeId;
  dynamic subscription;
  dynamic items;
  dynamic addresses;
  dynamic paymentModes;
  dynamic openingClosingDays;
  dynamic vouchars;
  dynamic orderTypes;
  dynamic merchantSubscriptions;
  dynamic modifiers;
  dynamic modifierGroups;
  dynamic orderRs;
  dynamic taxRates;
  dynamic merchantLogin;
  dynamic socialMediaLinks;
  TimeZone timeZone;
  Owner owner;
  MerchantLogo merchantLogo;
  dynamic accessToken;
  int isInstall;
  String merchantUid;
  int allowMultipleKoupon;
  bool allowMultiPay;
  String website;
  dynamic birthdayKouponUrl;
  dynamic mailchimpApiKey;
  int allowDeliveryTiming;
  CouponCode couponCode;
  EmailId emailId;
  dynamic deliverystatus;
  int tipsForPickup;
  int tipsForDilevery;
  bool activeTipsForPickup;
  bool activeTipForDilevery;
  dynamic ordersList;
  dynamic toMail;
  dynamic partnerName;

  factory Merchant.fromJson(Map<String, dynamic> json) => Merchant(
    id: json["id"],
    posMerchantId: json["posMerchantId"],
    name: merchantNameValues.map[json["name"]],
    employeePosId: json["employeePosId"],
    modileDeviceId: json["modileDeviceId"],
    isFbAppPublished: json["isFBAppPublished"],
    modieImeiCode: json["modieImeiCode"],
    allowAuxTax: json["allowAuxTax"],
    fBTabIds: json["fBTabIds"],
    allowReOrder: json["allowReOrder"],
    allowFutureOrder: json["allowFutureOrder"],
    activeCustomerFeedback: json["activeCustomerFeedback"],
    futureDaysAhead: json["futureDaysAhead"],
    createdDate: atedDateValues.map[json["createdDate"]],
    updatedDate: atedDateValues.map[json["updatedDate"]],
    phoneNumber: phoneNumberValues.map[json["phoneNumber"]],
    address: json["address"],
    action: json["action"],
    totalOrders: json["totalOrders"],
    cashOrder: json["cashOrder"],
    creditOrder: json["creditOrder"],
    pickup: json["pickup"],
    delivery: json["delivery"],
    totalOrdersPrice: json["totalOrdersPrice"],
    status: json["status"],
    storeId: json["storeId"],
    subscription: json["subscription"],
    items: json["items"],
    addresses: json["addresses"],
    paymentModes: json["paymentModes"],
    openingClosingDays: json["openingClosingDays"],
    vouchars: json["vouchars"],
    orderTypes: json["orderTypes"],
    merchantSubscriptions: json["merchantSubscriptions"],
    modifiers: json["modifiers"],
    modifierGroups: json["modifierGroups"],
    orderRs: json["orderRs"],
    taxRates: json["taxRates"],
    merchantLogin: json["merchantLogin"],
    socialMediaLinks: json["socialMediaLinks"],
    timeZone: TimeZone.fromJson(json["timeZone"]),
    owner: Owner.fromJson(json["owner"]),
    merchantLogo: merchantLogoValues.map[json["merchantLogo"]],
    accessToken: json["accessToken"],
    isInstall: json["isInstall"],
    merchantUid: json["merchantUid"],
    allowMultipleKoupon: json["allowMultipleKoupon"],
    allowMultiPay: json["allowMultiPay"],
    website: json["website"],
    birthdayKouponUrl: json["birthdayKouponUrl"],
    mailchimpApiKey: json["mailchimpApiKey"],
    allowDeliveryTiming: json["allowDeliveryTiming"],
    couponCode: couponCodeValues.map[json["couponCode"]],
    emailId: emailIdValues.map[json["emailId"]],
    deliverystatus: json["deliverystatus"],
    tipsForPickup: json["tipsForPickup"],
    tipsForDilevery: json["tipsForDilevery"],
    activeTipsForPickup: json["activeTipsForPickup"],
    activeTipForDilevery: json["activeTipForDilevery"],
    ordersList: json["ordersList"],
    toMail: json["toMail"],
    partnerName: json["partnerName"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "posMerchantId": posMerchantId,
    "name": merchantNameValues.reverse[name],
    "employeePosId": employeePosId,
    "modileDeviceId": modileDeviceId,
    "isFBAppPublished": isFbAppPublished,
    "modieImeiCode": modieImeiCode,
    "allowAuxTax": allowAuxTax,
    "fBTabIds": fBTabIds,
    "allowReOrder": allowReOrder,
    "allowFutureOrder": allowFutureOrder,
    "activeCustomerFeedback": activeCustomerFeedback,
    "futureDaysAhead": futureDaysAhead,
    "createdDate": atedDateValues.reverse[createdDate],
    "updatedDate": atedDateValues.reverse[updatedDate],
    "phoneNumber": phoneNumberValues.reverse[phoneNumber],
    "address": address,
    "action": action,
    "totalOrders": totalOrders,
    "cashOrder": cashOrder,
    "creditOrder": creditOrder,
    "pickup": pickup,
    "delivery": delivery,
    "totalOrdersPrice": totalOrdersPrice,
    "status": status,
    "storeId": storeId,
    "subscription": subscription,
    "items": items,
    "addresses": addresses,
    "paymentModes": paymentModes,
    "openingClosingDays": openingClosingDays,
    "vouchars": vouchars,
    "orderTypes": orderTypes,
    "merchantSubscriptions": merchantSubscriptions,
    "modifiers": modifiers,
    "modifierGroups": modifierGroups,
    "orderRs": orderRs,
    "taxRates": taxRates,
    "merchantLogin": merchantLogin,
    "socialMediaLinks": socialMediaLinks,
    "timeZone": timeZone.toJson(),
    "owner": owner.toJson(),
    "merchantLogo": merchantLogoValues.reverse[merchantLogo],
    "accessToken": accessToken,
    "isInstall": isInstall,
    "merchantUid": merchantUid,
    "allowMultipleKoupon": allowMultipleKoupon,
    "allowMultiPay": allowMultiPay,
    "website": website,
    "birthdayKouponUrl": birthdayKouponUrl,
    "mailchimpApiKey": mailchimpApiKey,
    "allowDeliveryTiming": allowDeliveryTiming,
    "couponCode": couponCodeValues.reverse[couponCode],
    "emailId": emailIdValues.reverse[emailId],
    "deliverystatus": deliverystatus,
    "tipsForPickup": tipsForPickup,
    "tipsForDilevery": tipsForDilevery,
    "activeTipsForPickup": activeTipsForPickup,
    "activeTipForDilevery": activeTipForDilevery,
    "ordersList": ordersList,
    "toMail": toMail,
    "partnerName": partnerName,
  };
}

enum CouponCode { ZL7_NBDKI6_S }

final couponCodeValues = EnumValues({
  "ZL7NBDKI6S": CouponCode.ZL7_NBDKI6_S
});

enum AtedDate { THE_11152018 }

final atedDateValues = EnumValues({
  "11-15-2018": AtedDate.THE_11152018
});

enum EmailId { SUMIT_A_GANGRADE_GMAIL_COM }

final emailIdValues = EnumValues({
  "sumit.a.gangrade@gmail.com": EmailId.SUMIT_A_GANGRADE_GMAIL_COM
});

enum MerchantLogo { FOODKONNEKT_MERCHAT_LOGO_301_KIDS_CATERING2_JPG }

final merchantLogoValues = EnumValues({
  "foodkonnekt_merchat_logo/301_KidsCatering2.jpg": MerchantLogo.FOODKONNEKT_MERCHAT_LOGO_301_KIDS_CATERING2_JPG
});

enum MerchantName { KIDS_CATERING_2 }

final merchantNameValues = EnumValues({
  "Kids Catering 2": MerchantName.KIDS_CATERING_2
});

class Owner {
  Owner({
    this.id,
    this.name,
    this.email,
    this.companyId,
    this.vendorUid,
    this.role,
    this.pos,
  });

  int id;
  OwnerName name;
  Email email;
  dynamic companyId;
  VendorUid vendorUid;
  Role role;
  Pos pos;

  factory Owner.fromJson(Map<String, dynamic> json) => Owner(
    id: json["id"],
    name: ownerNameValues.map[json["name"]],
    email: emailValues.map[json["email"]],
    companyId: json["companyId"],
    vendorUid: vendorUidValues.map[json["vendorUid"]],
    role: Role.fromJson(json["role"]),
    pos: Pos.fromJson(json["pos"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": ownerNameValues.reverse[name],
    "email": emailValues.reverse[email],
    "companyId": companyId,
    "vendorUid": vendorUidValues.reverse[vendorUid],
    "role": role.toJson(),
    "pos": pos.toJson(),
  };
}

enum Email { KIDS_CATREEN_MKONNEKT_COM }

final emailValues = EnumValues({
  "kidsCatreen@mkonnekt.com": Email.KIDS_CATREEN_MKONNEKT_COM
});

enum OwnerName { KIDS }

final ownerNameValues = EnumValues({
  "Kids": OwnerName.KIDS
});

class Pos {
  Pos({
    this.posId,
    this.name,
  });

  int posId;
  PosName name;

  factory Pos.fromJson(Map<String, dynamic> json) => Pos(
    posId: json["posId"],
    name: posNameValues.map[json["name"]],
  );

  Map<String, dynamic> toJson() => {
    "posId": posId,
    "name": posNameValues.reverse[name],
  };
}

enum PosName { NON_PO_S }

final posNameValues = EnumValues({
  "Non-PoS": PosName.NON_PO_S
});

class Role {
  Role({
    this.id,
    this.roleName,
    this.roleDescription,
  });

  int id;
  RoleName roleName;
  dynamic roleDescription;

  factory Role.fromJson(Map<String, dynamic> json) => Role(
    id: json["id"],
    roleName: roleNameValues.map[json["roleName"]],
    roleDescription: json["roleDescription"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "roleName": roleNameValues.reverse[roleName],
    "roleDescription": roleDescription,
  };
}

enum RoleName { SUPER_ADMIN }

final roleNameValues = EnumValues({
  "Super Admin": RoleName.SUPER_ADMIN
});

enum VendorUid { AD50_D52_D74_C19510_A1640_D67_CA91659214993456_B9_A46_E55_ED08_A68_CEC98_B309 }

final vendorUidValues = EnumValues({
  "ad50d52d74c19510a1640d67ca91659214993456b9a46e55ed08a68cec98b309": VendorUid.AD50_D52_D74_C19510_A1640_D67_CA91659214993456_B9_A46_E55_ED08_A68_CEC98_B309
});

enum PhoneNumber { THE_0000000000 }

final phoneNumberValues = EnumValues({
  "000-000-0000": PhoneNumber.THE_0000000000
});

class TimeZone {
  TimeZone({
    this.id,
    this.timeZoneName,
    this.timeZoneCode,
    this.hourDifference,
    this.minutDifference,
  });

  int id;
  dynamic timeZoneName;
  TimeZoneCode timeZoneCode;
  int hourDifference;
  int minutDifference;

  factory TimeZone.fromJson(Map<String, dynamic> json) => TimeZone(
    id: json["id"],
    timeZoneName: json["timeZoneName"],
    timeZoneCode: timeZoneCodeValues.map[json["timeZoneCode"]],
    hourDifference: json["hourDifference"],
    minutDifference: json["minutDifference"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "timeZoneName": timeZoneName,
    "timeZoneCode": timeZoneCodeValues.reverse[timeZoneCode],
    "hourDifference": hourDifference,
    "minutDifference": minutDifference,
  };
}

enum TimeZoneCode { AMERICA_DENVER }

final timeZoneCodeValues = EnumValues({
  "America/Denver": TimeZoneCode.AMERICA_DENVER
});

class MenuItem {
  MenuItem({
    this.id,
    this.isHidden,
    this.posItemId,
    this.originalPosItemId,
    this.name,
    this.description,
    this.taxAble,
    this.auxTaxAble,
    this.tempTaxAble,
    this.temAuxTaxAble,
    this.menuOrder,
    this.taxNames,
    this.price,
    this.priceType,
    this.unitName,
    this.isRevenue,
    this.isDefaultTaxRates,
    this.modifiedTime,
    this.categories,
    this.taxes,
    this.merchant,
    this.itemModifierGroups,
    this.orderItems,
    this.itemTimings,
    this.itemImage,
    this.categoriesName,
    this.itemModifierGroupsIds,
    this.itemModifierGroupsStatusIds,
    this.days,
    this.startTime,
    this.endTime,
    this.itemModifiersStatusIds,
    this.modifierLimits,
    this.isMinLimit,
    this.allowModifierLimit,
    this.allowItemTimings,
    this.allowModifierGroupOrder,
    this.modifierGroups,
    this.extras,
    this.categoryList,
    this.itemStatus,
    this.modifiersLimit,
    this.savedItemCountForExcelSheet,
    this.texesId,
    this.categoryItemIds,
  });

  int id;
  bool isHidden;
  String posItemId;
  String originalPosItemId;
  String name;
  String description;
  dynamic taxAble;
  dynamic auxTaxAble;
  dynamic tempTaxAble;
  dynamic temAuxTaxAble;
  dynamic menuOrder;
  dynamic taxNames;
  double price;
  PriceType priceType;
  dynamic unitName;
  dynamic isRevenue;
  bool isDefaultTaxRates;
  dynamic modifiedTime;
  List<dynamic> categories;
  dynamic taxes;
  Merchant merchant;
  dynamic itemModifierGroups;
  dynamic orderItems;
  dynamic itemTimings;
  dynamic itemImage;
  dynamic categoriesName;
  dynamic itemModifierGroupsIds;
  dynamic itemModifierGroupsStatusIds;
  dynamic days;
  dynamic startTime;
  dynamic endTime;
  dynamic itemModifiersStatusIds;
  dynamic modifierLimits;
  dynamic isMinLimit;
  dynamic allowModifierLimit;
  int allowItemTimings;
  dynamic allowModifierGroupOrder;
  dynamic modifierGroups;
  dynamic extras;
  dynamic categoryList;
  int itemStatus;
  dynamic modifiersLimit;
  dynamic savedItemCountForExcelSheet;
  dynamic texesId;
  dynamic categoryItemIds;

  factory MenuItem.fromJson(Map<String, dynamic> json) => MenuItem(
    id: json["id"],
    isHidden: json["isHidden"],
    posItemId: json["posItemId"],
    originalPosItemId: json["originalPosItemId"],
    name: json["name"],
    description: json["description"] == null ? null : json["description"],
    taxAble: json["taxAble"],
    auxTaxAble: json["auxTaxAble"],
    tempTaxAble: json["tempTaxAble"],
    temAuxTaxAble: json["temAuxTaxAble"],
    menuOrder: json["menuOrder"],
    taxNames: json["taxNames"],
    price: json["price"].toDouble(),
    priceType: priceTypeValues.map[json["priceType"]],
    unitName: json["unitName"],
    isRevenue: json["isRevenue"],
    isDefaultTaxRates: json["isDefaultTaxRates"],
    modifiedTime: json["modifiedTime"],
    categories: List<dynamic>.from(json["categories"].map((x) => x)),
    taxes: json["taxes"],
    merchant: Merchant.fromJson(json["merchant"]),
    itemModifierGroups: json["itemModifierGroups"],
    orderItems: json["orderItems"],
    itemTimings: json["itemTimings"],
    itemImage: json["itemImage"],
    categoriesName: json["categoriesName"],
    itemModifierGroupsIds: json["itemModifierGroupsIds"],
    itemModifierGroupsStatusIds: json["itemModifierGroupsStatusIds"],
    days: json["days"],
    startTime: json["startTime"],
    endTime: json["endTime"],
    itemModifiersStatusIds: json["itemModifiersStatusIds"],
    modifierLimits: json["modifierLimits"],
    isMinLimit: json["isMinLimit"],
    allowModifierLimit: json["allowModifierLimit"],
    allowItemTimings: json["allowItemTimings"],
    allowModifierGroupOrder: json["allowModifierGroupOrder"],
    modifierGroups: json["modifierGroups"],
    extras: json["extras"],
    categoryList: json["categoryList"],
    itemStatus: json["itemStatus"],
    modifiersLimit: json["modifiersLimit"],
    savedItemCountForExcelSheet: json["savedItemCountForExcelSheet"],
    texesId: json["texesId"],
    categoryItemIds: json["categoryItemIds"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "isHidden": isHidden,
    "posItemId": posItemId,
    "originalPosItemId": originalPosItemId,
    "name": name,
    "description": description == null ? null : description,
    "taxAble": taxAble,
    "auxTaxAble": auxTaxAble,
    "tempTaxAble": tempTaxAble,
    "temAuxTaxAble": temAuxTaxAble,
    "menuOrder": menuOrder,
    "taxNames": taxNames,
    "price": price,
    "priceType": priceTypeValues.reverse[priceType],
    "unitName": unitName,
    "isRevenue": isRevenue,
    "isDefaultTaxRates": isDefaultTaxRates,
    "modifiedTime": modifiedTime,
    "categories": List<dynamic>.from(categories.map((x) => x)),
    "taxes": taxes,
    "merchant": merchant.toJson(),
    "itemModifierGroups": itemModifierGroups,
    "orderItems": orderItems,
    "itemTimings": itemTimings,
    "itemImage": itemImage,
    "categoriesName": categoriesName,
    "itemModifierGroupsIds": itemModifierGroupsIds,
    "itemModifierGroupsStatusIds": itemModifierGroupsStatusIds,
    "days": days,
    "startTime": startTime,
    "endTime": endTime,
    "itemModifiersStatusIds": itemModifiersStatusIds,
    "modifierLimits": modifierLimits,
    "isMinLimit": isMinLimit,
    "allowModifierLimit": allowModifierLimit,
    "allowItemTimings": allowItemTimings,
    "allowModifierGroupOrder": allowModifierGroupOrder,
    "modifierGroups": modifierGroups,
    "extras": extras,
    "categoryList": categoryList,
    "itemStatus": itemStatus,
    "modifiersLimit": modifiersLimit,
    "savedItemCountForExcelSheet": savedItemCountForExcelSheet,
    "texesId": texesId,
    "categoryItemIds": categoryItemIds,
  };
}

enum PriceType { FIXED }

final priceTypeValues = EnumValues({
  "FIXED": PriceType.FIXED
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
