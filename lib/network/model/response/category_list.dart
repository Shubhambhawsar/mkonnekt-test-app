import 'dart:convert';
import '../models.dart';

CategoryList categoryListFromJson(String str) => CategoryList.fromJson(json.decode(str));

String categoryListToJson(CategoryList data) => json.encode(data.toJson());

class CategoryList {
  CategoryList({
    this.status,
    this.message,
    this.categories,
  });

  int status;
  String message;
  List<Category> categories;

  factory CategoryList.fromJson(Map<String, dynamic> json) => CategoryList(
    status: json["status"],
    message: json["message"],
    categories: List<Category>.from(json["categories"].map((x) => Category.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "categories": List<dynamic>.from(categories.map((x) => x.toJson())),
  };
}