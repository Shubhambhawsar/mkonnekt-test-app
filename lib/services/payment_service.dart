import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:mkonnekt_kitchen/data/my_strings.dart';
import 'package:stripe_payment/stripe_payment.dart';

class StripeTransactionResponse {
  String message;
  bool success;

  StripeTransactionResponse(this.message, this.success);
}

class StripeService {
    static String baseUrl = "https://api.stripe.com/v1";
    static String paymentApiUrl = "${StripeService.baseUrl}/payment_intents";
    static String secret = MyStrings.STRIPE_SECRET_KEY;
    static Map<String, String> headers = {
      'Authorization': 'Bearer ${StripeService.secret}',
      'Content-Type': 'application/x-www-form-urlencoded',
    };

    static init() {
      StripePayment.setOptions(StripeOptions(publishableKey: MyStrings.STRIPE_PUBLISHED_KEY, merchantId: "Test", androidPayMode: 'test'));
    }

    static Future<StripeTransactionResponse> paymentViaNewCard(String amount, String currency) async {
      try{
        var paymentMethod = await StripePayment.paymentRequestWithCardForm(CardFormPaymentRequest());
        print('payment method' + jsonEncode(paymentMethod));
        var paymentIntent = await StripeService.createPaymentIntent(amount, currency);

        var response = await StripePayment.confirmPaymentIntent(
            PaymentIntent(
                clientSecret: paymentIntent['client_secret'],
              paymentMethodId: paymentMethod.id,
            )
        );
        if (response.status == 'succeeded') {
          return new StripeTransactionResponse( "Transaction Successful", true);
        } else
          return new StripeTransactionResponse( "Transaction Failed : ${response.status}", false);

      } catch(error) {
        return new StripeTransactionResponse( "Transaction Failed error: ${error.toString()}", false);
      }
    }

    static Future<Map<String, dynamic>> createPaymentIntent(String amount, String currency) async{
      try {
        Map<String, dynamic> body = {
          'amount': amount,
          'currency': currency,
          'payment_method_types[]': 'card'
        };
        var response = await http.post(
            StripeService.paymentApiUrl,
            body: body,
          headers: StripeService.headers
        );
        return jsonDecode(response.body);
      } catch(error) {
        print('createPayment error ' + error.toString());
      }
      return null;
    }
}