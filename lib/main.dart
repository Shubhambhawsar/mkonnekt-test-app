import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mkonnekt_kitchen/data/my_colors.dart';
import 'package:mkonnekt_kitchen/data/my_strings.dart';
import 'package:mkonnekt_kitchen/data/utills.dart';
import 'package:mkonnekt_kitchen/ui/activities/home.dart';
import 'package:mkonnekt_kitchen/ui/activities/login.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'mKonnekt Kitchen',
      theme: ThemeData(
        primaryColor: MyColors.colorWhite,
        primaryColorDark: MyColors.primaryDark,
        primaryColorLight: MyColors.primaryLight,
      ),
      home: SplashScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class SplashScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return SplashState();
  }
}

class SplashState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    splashScreenHandler();
  }

  splashScreenHandler() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(MyStrings.TOKEN))
      Timer(Duration(seconds: 3), ()=>Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) =>  HomeScreen())));
    else
      Timer(Duration(seconds: 3), ()=>Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) =>  LoginScreen())));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyColors.colorWhite,
        body: Align(
          child: Container(
            width: 200,
            child: Image.asset(Utility.getImage('food_konnekt_logo.png'), fit: BoxFit.cover),
          ),
        ),
      ),
    );
  }
}